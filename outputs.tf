output "subnet_id"{
    value = azurerm_subnet.web-servers.id
}
output "network_name"{
    value = azurerm_virtual_network.vnet.name
}
