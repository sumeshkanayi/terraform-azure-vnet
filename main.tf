resource "azurerm_virtual_network" "vnet" {
  name     = var.network_name
  location = var.location
  resource_group_name = var.resource_group_name
  address_space       = var.address_space
  tags = var.tags
}
resource "azurerm_subnet" "web-servers" {
  name                 = "web-servers"
  resource_group_name  = var.resource_group_name
  virtual_network_name = var.network_name
  address_prefixes     = ["10.0.2.0/24"]

}
